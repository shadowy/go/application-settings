module gitlab.com/shadowy/go/application-settings

go 1.12

require (
	github.com/fsnotify/fsnotify v1.4.7
	github.com/rs/zerolog v1.21.0
	gitlab.com/shadowy/go/health-checker v0.1.1
	gopkg.in/yaml.v3 v3.0.0-20190709130402-674ba3eaed22
)
