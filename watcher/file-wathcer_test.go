package watcher

import "testing"

func TestFileWatcher_Setup(t *testing.T) {
	t.Parallel()

	file := NewFileWatcher("test")
	err := file.Setup(nil, func(data string) (result interface{}, err error) {
		return
	})
	if err != nil {
		t.Error(err)
	}
}
