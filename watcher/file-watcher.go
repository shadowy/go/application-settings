package watcher

import (
	"github.com/fsnotify/fsnotify"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/shadowy/go/application-settings/settings"
	"io/ioutil"
)

// FileWatcher - watch file with settings and update it in application
type FileWatcher struct {
	fileName string
	config   *settings.Configuration
	creator  settings.WatcherSettingsCreator
	log      zerolog.Logger
}

// NewFileWatcher - create new file watcher
func NewFileWatcher(fileName string) *FileWatcher {
	result := FileWatcher{fileName: fileName}
	result.log = log.Logger.With().Str("fileName", fileName).Logger()
	return &result
}

// Setup - setup settings for watcher
func (watcher *FileWatcher) Setup(cfg *settings.Configuration, creator settings.WatcherSettingsCreator) (err error) {
	watcher.log.Debug().Msg("FileWatcher.Setup")
	watcher.config = cfg
	watcher.creator = creator
	watch, err := fsnotify.NewWatcher()
	if err != nil {
		watcher.log.Error().Stack().Err(err).Msg("FileWatcher.Setup")
	}
	go watcher.process(watch)

	return watcher.read()
}

func (watcher FileWatcher) process(watch *fsnotify.Watcher) {
	for {
		select {
		case event, ok := <-watch.Events:
			if !ok {
				return
			}
			if event.Op&fsnotify.Write == fsnotify.Write {
				watcher.log.Debug().Msg("FileWatcher.process watch.Event write")
				watcher.read()
			}
		case err, ok := <-watch.Errors:
			if !ok {
				return
			}
			if err != nil {
				watcher.log.Error().Stack().Err(err).Msg("FileWatcher.process watch.error")
			}
		}
	}
}

func (watcher FileWatcher) read() error {
	watcher.log.Debug().Msg("FileWatcher.read")
	data, err := ioutil.ReadFile(watcher.fileName)
	if err != nil {
		watcher.log.Error().Stack().Err(err).Msg("FileWatcher.read ReadFile")
		return err
	}
	cfg, err := watcher.creator(string(data))
	if err != nil {
		watcher.log.Error().Stack().Err(err).Msg("FileWatcher.read creator")
		return err
	}
	watcher.config.Set(cfg)
	return nil
}
