package settings

import (
	"bytes"
	"io"
	"testing"
)

type TestApplicationConfig struct {
	ID   int `yaml:"id"`
	Text string
}

func (setting TestApplicationConfig) FromReader(data io.Reader) (result *TestApplicationConfig, err error) {
	r, err := ApplicationSettings.FromReader(&TestApplicationConfig{}, data)
	if err != nil {
		return
	}
	result = r.(*TestApplicationConfig)
	return
}

func TestMain_Check(t *testing.T) {
	t.Parallel()
	defer func() {
		if err := recover(); err != nil {
			t.Error(err)
		}
	}()

	_, _ = TestApplicationConfig{}.FromReader(bytes.NewReader([]byte("id: 10")))
}
