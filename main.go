package settings

import "gitlab.com/shadowy/go/application-settings/settings"

// ApplicationSettings - instance of ApplicationSettings
var ApplicationSettings settings.ApplicationSettings
