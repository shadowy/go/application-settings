<a name="unreleased"></a>
## [Unreleased]


<a name="v1.0.12"></a>
## [v1.0.12] - 2019-11-06
### Features
- add subscription to change value


<a name="v1.0.11"></a>
## [v1.0.11] - 2019-08-06
### Bug Fixes
- issue with not ptr structure


<a name="v1.0.10"></a>
## [v1.0.10] - 2019-07-25
### Bug Fixes
- fix issue regarding init settings when field is nil


<a name="v1.0.9"></a>
## [v1.0.9] - 2019-07-25
### Bug Fixes
- fix issue regarding init settings when field is nil


<a name="v1.0.8"></a>
## [v1.0.8] - 2019-07-25

<a name="v1.0.7"></a>
## [v1.0.7] - 2019-07-23
### Code Refactoring
- change package name


<a name="v1.0.6"></a>
## [v1.0.6] - 2019-07-23
### Code Refactoring
- made easier creation of FileWatcher


<a name="v1.0.5"></a>
## [v1.0.5] - 2019-07-22

<a name="v1.0.4"></a>
## [v1.0.4] - 2019-07-18

<a name="v1.0.3"></a>
## [v1.0.3] - 2019-07-15

<a name="v1.0.2"></a>
## [v1.0.2] - 2019-07-15

<a name="v1.0.1"></a>
## v1.0.1 - 2019-07-12
### Features
- basic functionality


[Unreleased]: https://gitlab.com/shadowy/go/application-settings/compare/v1.0.12...HEAD
[v1.0.12]: https://gitlab.com/shadowy/go/application-settings/compare/v1.0.11...v1.0.12
[v1.0.11]: https://gitlab.com/shadowy/go/application-settings/compare/v1.0.10...v1.0.11
[v1.0.10]: https://gitlab.com/shadowy/go/application-settings/compare/v1.0.9...v1.0.10
[v1.0.9]: https://gitlab.com/shadowy/go/application-settings/compare/v1.0.8...v1.0.9
[v1.0.8]: https://gitlab.com/shadowy/go/application-settings/compare/v1.0.7...v1.0.8
[v1.0.7]: https://gitlab.com/shadowy/go/application-settings/compare/v1.0.6...v1.0.7
[v1.0.6]: https://gitlab.com/shadowy/go/application-settings/compare/v1.0.5...v1.0.6
[v1.0.5]: https://gitlab.com/shadowy/go/application-settings/compare/v1.0.4...v1.0.5
[v1.0.4]: https://gitlab.com/shadowy/go/application-settings/compare/v1.0.3...v1.0.4
[v1.0.3]: https://gitlab.com/shadowy/go/application-settings/compare/v1.0.2...v1.0.3
[v1.0.2]: https://gitlab.com/shadowy/go/application-settings/compare/v1.0.1...v1.0.2
