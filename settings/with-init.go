package settings

// WithInit - Interface for initiate settings after it loaded
type WithInit interface {
	Init() error
}
