package settings

import (
	"bytes"
	"errors"
	"io"
	"reflect"
	"testing"
)

const testString = "Test"

func (setting *TestApplicationConfig) Init() (err error) {
	setting.Text += testString
	return
}

func (setting TestApplicationConfig1) FromReader(data io.Reader) (result *TestApplicationConfig1, err error) {
	r, err := ApplicationSettings{}.FromReader(&TestApplicationConfig1{}, data)
	if err != nil {
		return
	}
	result = r.(*TestApplicationConfig1)
	return
}

var (
	object, errorInit = TestApplicationConfig{}.FromReader(bytes.NewReader([]byte("id: 10")))
)

func TestApplicationSettings_CreateFromStringError(t *testing.T) {
	t.Parallel()

	_, err := TestApplicationConfig{}.FromReader(bytes.NewReader([]byte("id10")))
	if err == nil {
		t.Error("Should throw error")
	}
}

func TestApplicationSettings_CreateFromString(t *testing.T) {
	t.Parallel()

	if errorInit != nil {
		t.Error(errorInit)
	}
}

func TestApplicationSettings_TestId(t *testing.T) {
	t.Parallel()

	if object == nil {
		t.Error("object is nil")
		return
	}
	if object.ID != 10 {
		t.Error("ID != 10")
		return
	}
}

func TestApplicationSettings_TestText(t *testing.T) {
	t.Parallel()

	if object == nil {
		t.Error("object is nil")
		return
	}
	if object.Text != testString {
		t.Error("Text != 'Test'")
		return
	}
}

func TestApplicationSettings_CreateFromStringComplex(t *testing.T) {
	t.Parallel()

	r, err := TestApplicationConfig1{}.FromReader(bytes.NewReader([]byte("main:\n   id: 10\n")))
	if err != nil {
		t.Error(err)
	}

	if r.Main == nil || r.Main.Text != testString {
		t.Error("Text != 'Test'")
		return
	}
}

func TestApplicationSettings_GetSettings(t *testing.T) {
	t.Parallel()

	cfg, err := ApplicationSettings{}.GetSettings(reflect.TypeOf(TestApplicationConfig{}), new(TestMonitoring))
	if err != nil {
		t.Error(err)
	}
	if cfg == nil {
		t.Error("cfg is null")
	}
	if cfg.Config().(*TestApplicationConfig).ID != 10 {
		t.Error("cfg.Config().ID != 10")
	}
}

func TestApplicationSettings_GetSettingsWithError(t *testing.T) {
	t.Parallel()

	cfg, err := ApplicationSettings{}.GetSettings(reflect.TypeOf(TestApplicationConfig{}), new(TestMonitoringError))
	if err == nil {
		t.Error("err not defined")
	}
	if cfg != nil {
		t.Error("cfg is not null")
	}
}

type TestApplicationConfig struct {
	ID   int `yaml:"id"`
	Text string
}

type TestApplicationConfig1 struct {
	Main *TestApplicationConfig `yaml:"main"`
	Text string                 `yaml:"text"`
}

func (setting TestApplicationConfig) FromReader(data io.Reader) (result *TestApplicationConfig, err error) {
	r, err := ApplicationSettings{}.FromReader(&TestApplicationConfig{}, data)
	if err != nil {
		return
	}
	result = r.(*TestApplicationConfig)
	return
}

type TestMonitoring struct {
}

func (mon TestMonitoring) Setup(cfg *Configuration, creator WatcherSettingsCreator) error {
	v, err := creator("id: 10")
	cfg.Set(v)
	return err
}

type TestMonitoringError struct {
}

func (mon TestMonitoringError) Setup(cfg *Configuration, creator WatcherSettingsCreator) error {
	return errors.New("test error")
}
