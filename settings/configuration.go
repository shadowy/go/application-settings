package settings

// Configuration - provide access to latest settings
type Configuration struct {
	config interface{}

	events []*EventConfiguration
}

// Config - latest settings
func (cfg Configuration) Config() interface{} {
	return cfg.config
}

// Set - set settings
func (cfg *Configuration) Set(config interface{}) {
	cfg.config = config
	cfg.emit()
}

// Subscribe - subscribe to event when config changed
func (cfg *Configuration) Subscribe() chan *Configuration {
	event := CreateEventConfiguration()
	cfg.events = append(cfg.events, event)
	return event.Channel
}

func (cfg *Configuration) emit() {
	for i := range cfg.events {
		item := cfg.events[i]
		item.Channel <- cfg
	}
}
