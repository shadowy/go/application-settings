package settings

type EventConfiguration struct {
	Channel chan *Configuration
}

func CreateEventConfiguration() *EventConfiguration {
	return &EventConfiguration{Channel: make(chan *Configuration)}
}
