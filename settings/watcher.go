package settings

// WatcherSettingsCreator - create settings from the string
type WatcherSettingsCreator func(data string) (result interface{}, err error)

// Watcher - interface for watching settings from source and update it in configuration
type Watcher interface {
	Setup(cfg *Configuration, creator WatcherSettingsCreator) error
}
