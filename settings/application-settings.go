package settings

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/shadowy/go/health-checker"
	"gopkg.in/yaml.v3"
	"io"
	"io/ioutil"
	"os"
	"reflect"
)

// ApplicationSettings - provide loading and updating application settings
type ApplicationSettings struct {
}

// GetHeathChecker - provide list heath checkers for settings
func (applicationSettings *ApplicationSettings) GetHeathChecker() []health.Checker {
	return health.GetHeathChecker(applicationSettings)
}

// FromString - Load settings from string/
func (applicationSettings ApplicationSettings) FromString(obj interface{}, data string) (result interface{}, err error) {
	err = yaml.Unmarshal([]byte(data), obj)
	if err != nil {
		log.Logger.Error().Stack().Err(err).Msg("ApplicationSettings.FromString")
		return
	}

	err = applicationSettings.callInit(obj)
	if err != nil {
		log.Logger.Error().Stack().Err(err).Msg("ApplicationSettings.FromString callInit")
		return
	}

	result = obj
	return
}

// FromReader - Load settings from reader
func (applicationSettings ApplicationSettings) FromReader(obj interface{}, reader io.Reader) (result interface{}, err error) {
	content, err := ioutil.ReadAll(reader)
	if err != nil {
		log.Logger.Error().Stack().Err(err).Msg("ApplicationSettings.FromReader")
		return
	}
	result, err = applicationSettings.FromString(obj, string(content))
	return
}

// FromFile - Load settings from file
func (applicationSettings ApplicationSettings) FromFile(obj interface{}, fileName string) (result interface{}, err error) {
	file, err := os.Open(fileName)

	if err != nil {
		log.Logger.Error().Stack().Err(err).Msg("ApplicationSettings.FromFile")
		return
	}
	defer func() { _ = file.Close() }()

	result, err = applicationSettings.FromReader(obj, file)
	return
}

// GetSettings - Get settings with monitoring of changes
func (applicationSettings ApplicationSettings) GetSettings(objType reflect.Type, watcher Watcher) (result *Configuration, err error) {
	config := Configuration{}
	err = watcher.Setup(&config, func(data string) (result interface{}, err error) {
		res := reflect.New(objType).Interface()
		return applicationSettings.FromString(res, data)
	})
	if err != nil {
		log.Logger.Error().Stack().Err(err).Msg("ApplicationSettings.GetSettings")
		return
	}
	result = &config
	return
}

func (applicationSettings ApplicationSettings) getWithInit(obj interface{}) WithInit {
	switch v := obj.(type) {
	case WithInit:
		return v
	default:
		return nil
	}
}

func (applicationSettings ApplicationSettings) callInit(obj interface{}) (err error) {
	if withInit := applicationSettings.getWithInit(obj); withInit != nil {
		err = withInit.Init()
		return
	}

	r := reflect.ValueOf(obj)
	if r.Type().Kind() == reflect.Ptr {
		r = reflect.Indirect(r)
	}
	if r.Type().Kind() != reflect.Struct {
		return
	}

	count := r.NumField()
	for i := 0; i < count; i++ {
		if r.Field(i).Type().Kind() == reflect.Ptr && r.Field(i).IsNil() {
			continue
		}
		v := r.Field(i).Interface()
		err = applicationSettings.callInit(v)
		if err != nil {
			return
		}
	}
	return nil
}
