package settings

import "testing"

func TestConfiguration_Config(t *testing.T) {
	t.Parallel()

	settings := Configuration{}
	settings.Set(1)
	if settings.Config().(int) != 1 {
		t.Error("settings.Config() != 1")
	}
}
